import requests

SEARCH_URL = 'https://www.googleapis.com/youtube/v3/search'
PARAM_DEVELOPER_KEY = "AIzaSyD4wIKoxg645dCjM-PxuOJ6nqZpkJ-3d5w"
PARAM_PART = 'id,snippet'
PARAM_ORDER = 'relevance'
PARAM_SEARCH_TERMS = 'music'
PARAM_TYPE = 'video'
PARAM_MAX_RESULTS = 5

searchParams = {'part' : PARAM_PART,
    'key' : PARAM_DEVELOPER_KEY,
    'q' : PARAM_SEARCH_TERMS,
    'type' : PARAM_TYPE,
    'order' : PARAM_ORDER,
    'maxResults' : PARAM_MAX_RESULTS}

r = requests.get(SEARCH_URL, params = searchParams)
data = r.json()
print(data)
items = data.get('items')
for item in items:
    print('VideoID: ' + item['id']['videoId'])
    print('Published At: ' + item['snippet']['publishedAt'])
    print('Title: ' + item['snippet']['title'])
